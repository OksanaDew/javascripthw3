//В программровании нужны функции для того, чтобы в разных местах кода не повторять снова и снова один и тот же участок кода,
//его можно оформить в виде функции, которую можно будет вызвать всего одной строкой с ее названием неограниченое количество раз
// при этом сокращая общую длину кода.
//Аргумент в функциюнужно передавать для того, чтобы один и тот же код можно было использовать
// в разных ситуациях и получать требуемый результат, а не один и тот же.

let num1, num2, oper;
let prevOper = null;

    do {
        num1 = prompt('Enter number 1: ');
    } while (!num1 || num1 === ' ' || isNaN(+num1));
    do {
        oper = prompt('Enter operation: ');
    } while (oper !== '+' && oper !== '-' && oper !== '*' && oper !== '/');
    prevOper = oper;
    do {
        num2 = prompt('Enter number 2: ');
    } while (!num2 || num2 === ' ' || isNaN(+num2));

    resultOper(num1, oper, num2);

    function resultOper(num1, oper, num2) {
        num1 = num1 || 0;
        oper = oper || '+';
        num2 = num2 || 0;
        let result = null;
        switch (oper) {
            case '+':
                result = +num1 + +num2;
                break;
            case '-':
                result = num1 - num2;
                break;
            case '*':
                result = num1 * num2;
                break;
            case '/':
                result = num1 / num2;
                break;
        }
        console.log(result);
    }

